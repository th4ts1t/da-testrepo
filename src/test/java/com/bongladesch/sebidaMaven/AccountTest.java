package com.bongladesch.sebidaMaven;

import java.util.Date;

import com.bongladesch.sebidaMaven.accounts.Account;
import com.bongladesch.sebidaMaven.accounts.Address;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Unit test for account app.
 */
public class AccountTest extends TestCase
{
	private static Logger logger = LogManager.getLogger( AccountTest.class );
	
	private Account a;
	private Account b;
	private Account c;
	
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    @SuppressWarnings("deprecation")
	public AccountTest(String testName)
    {
    	super(testName);
		logger.info("Test");
    	Date aBirthday = new Date(1992, 4, 13);
    	Address aAddress = new Address("Laufweg", 12, "73850", "Doofhausen");
    	a = new Account("Max", "Mustermann", aBirthday, aAddress);
    	aBirthday = new Date(1990, 1, 20);
    	a.deposit(500);
    	aAddress = new Address("Gehweg", 9, "05350", "Dummenhausen");
    	b = new Account("Maria", "Musterfrau", aBirthday, aAddress);
    	b.deposit(1000);
    	aBirthday = new Date(1976, 5, 4);
    	aAddress = new Address("Radweg", 110, "73430", "Blödstadt");
    	c = new Account("Martin", "Mustermartin", aBirthday, aAddress);
    	c.deposit(1500);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
		logger.info("Test suite ausgefuehrt");
        return new TestSuite(AccountTest.class);
    }

    /* Test Cases */
    
    public void testEquals()
    {
        assertFalse(a.equals(b));
		logger.info("Test testEquals ausgefuehrt");
    }
    
    public void testDeposit()
    {
    	assertTrue(a.getBalance() == 500);
		logger.info("Test testDeposit ausgefuehrt");
    }
    
    public void testTransfer1()
    {
    	b.transfer(1000, a);
    	assertTrue(b.getBalance() == 0);
		logger.info("Test testTransfer1 ausgefuehrt");
    }
    
    public void testTransfer2()
    {
    	b.transfer(1000, a);
    	assertTrue(a.getBalance() == 1500);
		logger.info("Test testTransfer2 ausgefuehrt");
    }
    
    public void testWithdraw1()
    {
    	assertFalse(c.withdraw(1501));
		logger.info("Test testWithdraw1 ausgefuehrt");
    }
    
    public void testWithdraw2()
    {
    	c.withdraw(1499);
    	assertTrue(c.getBalance() == 1);
		logger.info("Test testWithdraw2 ausgefuehrt");
    }
    
    public void testAppMain()
    {
    	App.main(new String[1]);
		logger.info("Test testAppMain ausgefuehrt");
    }
	
	public void testLogger()
	{
		logger.debug("Debug-Meldung");
		logger.info("Info-Meldung");
		logger.warn("Warn-Meldung");
		logger.error("Error-Meldung");
		logger.fatal("Fatal-Meldung");
	}
}
