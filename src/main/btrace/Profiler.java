//originale: https://dzone.com/articles/btrace-hidden-gem-java

import com.sun.btrace.*;
import com.sun.btrace.annotations.*;
import static com.sun.btrace.BTraceUtils.*;

@BTrace
public class Profiler{
    @TLS private static String method;

    @OnMethod(
            clazz = "/.*com\\.bongladesch\\.sebidaMaven.*/",
            method = "/.*/"
    )
    public static void onMethod(
            @ProbeClassName String className,
            @ProbeMethodName String probeMethod,
            AnyType[] args ) {
        method = strcat( strcat( className, "," ), probeMethod );
    }

    @OnMethod(
            clazz = "/.*com\\.bongladesch\\.sebidaMaven.*/",
            method = "/.*/",
            location = @Location( Kind.RETURN )
    )
    public static void onMethodReturn( @Duration long duration ) {
        //println(strcat(method , strcat("," + str(duration/1000))));

        println(strcat(strcat(method,","),str(duration / 1000)));
    }
}