package com.bongladesch.sebidaMaven;

import java.util.Date;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.bongladesch.sebidaMaven.accounts.Account;
import com.bongladesch.sebidaMaven.accounts.Address;

/**
 * Account app main method.
 */
public class App 
{
	//Initialisierung eines Loggers
	private static Logger logger = LogManager.getLogger(App.class);
	
	private static Account a;
	private static Account b;
	private static Account c;
	
    @SuppressWarnings("deprecation")
	public static void main(String[] args)
    {
    	// date format year, month, date
    	Date aBirthday = new Date(1992, 4, 13);
    	Address aAddress = new Address("Laufweg", 12, "73850", "Doofhausen");
    	a = new Account("Max", "Mustermann", aBirthday, aAddress);
    	aBirthday = new Date(1990, 1, 20);
    	aAddress = new Address("Gehweg", 9, "05350", "Dummenhausen");
    	b = new Account("Maria", "Musterfrau", aBirthday, aAddress);
    	aBirthday = new Date(1976, 5, 4);
    	aAddress = new Address("Radweg", 110, "73430", "Blödstadt");
    	c = new Account("Martin", "Mustermartin", aBirthday, aAddress);
    	
    	runSample();
		logger.info("Main-Methode wurde durchlaufen");
    }
    
    private static void runSample()
    {
    	a.deposit(400);
    	b.deposit(1000);
    	c.deposit(12000);
    	c.transfer(3050, a);
    	System.out.println(a);
    	System.out.println(b);
    	System.out.println(c);
		logger.info("runSample-Methode wurde durchlaufen");
    }
}
