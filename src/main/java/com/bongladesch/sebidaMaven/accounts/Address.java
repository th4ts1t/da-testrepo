package com.bongladesch.sebidaMaven.accounts;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Address
{
	//Initialisierung eines Loggers
	private static Logger logger = LogManager.getLogger(Address.class);

	private String street;
	private int number;
	private String zipcode;
	private String city;
	
	public Address(String s, int n, String z, String c)
	{
		this.setStreet(s);
		this.setNumber(n);
		this.setZipcode(z);
		this.setCity(c);
		logger.info("Adresse wurde erstellt");
		logger.debug("Strasse: " + street);
	}

	public String getStreet()
	{
		return street;
	}

	public void setStreet(String street)
	{
		this.street = street;
	}

	public int getNumber()
	{
		return number;
	}

	public void setNumber(int number)
	{
		this.number = number;
	}

	public String getZipcode()
	{
		return zipcode;
	}

	public void setZipcode(String zipcode)
	{
		this.zipcode = zipcode;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}
	
	@Override
	public String toString()
	{
		String s = "\t\tStrasse: " + getStreet();
		String n = "\n\t\tHausnummer: " + getNumber();
		String z = "\n\t\tPostleitzahl: " + getZipcode();
		String c = "\n\t\tStadt: " + getCity();
		return s + n + z + c;
	}
	
}
