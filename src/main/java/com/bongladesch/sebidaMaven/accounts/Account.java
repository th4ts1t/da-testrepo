package com.bongladesch.sebidaMaven.accounts;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Date;

/**
 * Account class contains customer data
 * and all methods for booking.
 * @author bongaldesch
 *
 */
public class Account
{
	//Initialisierung eines Loggers
	private static Logger logger = LogManager.getLogger( Account.class );
	
	private static long nextNumber = 0;

	// Account holder data
	Customer holder;
	
	// Account data
	private long number;
	private double balance;
	
	/* Constructor for Accounts */
	public Account(String fn, String ln, Date bd, Address address)
	{
		setHolder(new Customer(fn, ln, bd, address));
		setNumber(++nextNumber);
		setBalance(0);
		logger.info("Account " + number + " wurde erstellt");
		logger.debug("Accountguthaben: " + balance);
	}
	
	/* Getter and Setter */
	
	public double getBalance()
	{
		return this.balance;
	}
	
	public void setBalance(double balance)
	{
		this.balance = balance;
	}
	
	public Customer getHolder()
	{
		return this.holder;
	}
	
	public void setHolder(Customer holder)
	{
		this.holder = holder;
	}
	
	public long getNumber()
	{
		return this.number;
	}
	
	public void setNumber(long number)
	{
		this.number = number;
	}
	
	/* Booking methods */
	
	public void deposit(double amount)
	{
		this.balance += amount;
	}
	
	public boolean withdraw(double amount)
	{
		if(this.balance - amount < 0) 
		{
			logger.error("Betrag konnte nicht abgehoben werden");
			return false;
		}
		this.balance -= amount;
		logger.info("Betrag von " + amount + " konnte abgehoben werden");
		return true;
	}
	
	public boolean transfer(double amount, Account other)
	{
		if(withdraw(amount))
		{
			other.deposit(amount);
			this.withdraw(amount);
			logger.info("Betrag von " + amount + " konnte überwiesen werden");
			return true;
		}
		logger.error("Betrag von " + amount + " konnte nicht überwiesen werden");
		return false;
	}
	
	/* Standard methods */

	@Override
	public String toString()
	{
		String n = "\nKontonummer: " + getNumber();
		String b = "\nKontostand: " + getBalance() + "€\n";
		String i = "Inhaber:\n" + getHolder();
		return n + b + i;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if(!(other instanceof Account)) return false;
		Account that = (Account) other;
		if(this.getNumber() == that.getNumber()) return true;
		return false;
	}
}
